import React from 'react';
import {Route, BrowserRouter} from 'react-router-dom';

import Home from './pages/Home';
import PDF from './pages/PDF';

const Routes = ()=>{
    return(
        <BrowserRouter>
        <Route component={Home} path="/" exact/>
        <Route component={PDF} path="/PDF"/>
        </BrowserRouter>
    );
}

export default Routes;