import React from 'react';

import ReactPDF, {
  Document,
  Page,
  Text,
  View,
  Image,
  StyleSheet,
  Font, PDFDownloadLink, PDFViewer 
} from '@react-pdf/renderer';

var QRCode = require('qrcode.react');


const styles = StyleSheet.create({
  page: {
    alignItems:'center',
    justifyContent:'center'
  },
  image: {
    width: '50%',
  },
  textWrapper: {
    width: '100%',
    height: '20%',
    alignItems:'center'
  },
  text: {
    fontFamily: 'Oswald',
    color: '#212121',
    fontSize:50
  },
});

Font.register({
    family: 'Oswald',
    src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf',
});


const items = [
  {
      "quant_qrs": "7",
      "validacao": "@PETEngComp",
      "id": "1",
      "lugar": "PET Eng. Comp.",
      "desc": "Onde você pode consultar livros e provas antigas, tirar dúvidas sobre programação, entre outras coisas.",
      "dica": "Sala do grupo que organiza a gincana, bem massas! :D Venha pegar seu prêmio!",
      "dica_prox": "Tenho lanche, almoço e café, e passo cartão!"
  },
  {
      "quant_qrs": "7",
      "validacao": "@PETEngComp",
      "id": "2",
      "lugar": "Cantina do CT",
      "desc": "Onde muitas vezes se descança das aulas. Útil para almoçar e lanchar quando está com pressa!",
      "dica": "Tenho lanche, almoço e café, e passo cartão!",
      "dica_prox": "Onde vc compra ingressos para eventos da Engenharia."
  }
]


function qrImg(){
  const canvas = document.getElementById("qrcode");
  const pngUrl = canvas
    .toDataURL("image/png")
    .replace("image/png", "image/octet-stream");


return pngUrl;
}


 const PDF = ()=>{
  return(
    <>
      <PDFViewer height={900} width={800} >
        <Document>
        {items.map(item => (
          <Page key={item.id} style={styles.page} size="A4" page>
            <View style={styles.textWrapper}>
              <Text style={styles.text}>
              {item.id}. {item.lugar}
              </Text>
            </View>
            <View style={styles.image}>
              <Image src={qrImg}/>
            </View>
          </Page>
          ))}
        </Document>
      </PDFViewer>

      <QRCode 
        id="qrcode"
        value={JSON.stringify(items[0])}
        includeMargin={true}
        size={300}
        />
    </>
  );
}


// Renders document and save it
//ReactPDF.render(PDF, `${__dirname}/output.pdf`);

export default PDF;