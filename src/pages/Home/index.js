import React  from 'react';
import './styles.css';
import PDF from '../PDF';
import Qr from '../../assets/qrcode.png'
import {Link} from 'react-router-dom';
import {Document,Page, Text,  View,  Image,  StyleSheet,  Font,PDFViewer } from '@react-pdf/renderer';

var QRCode = require('qrcode.react');

const styles = StyleSheet.create({
  page: {
    alignItems:'center',
    justifyContent:'center'
  },
  image: {
    width: '85%',
  },
  logo: {
    width: '30%',
  },
  textWrapper: {
    // width: '100%',
    // height: '20%',
    alignItems:'center'
    
  },
  text: {
    fontFamily: 'Oswald',
    color: '#212121',
    fontSize:35
  },
});

Font.register({
    family: 'Oswald',
    src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf',
});

function App() {
  
  const [items, setItems] = React.useState([]);
  const [text, setText] = React.useState('');
  var qr;

  async function handleSubmit(event){
    setItems(JSON.parse(text));
    event.preventDefault();
  }

  function handleInputChange(event){
    setText(event.target.value); 
  }

  function downloadQR (id) {
    const canvas = document.getElementById(id);
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = id +".png";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }

  function qrImg(id){
    const canvas = document.getElementById(id);
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");

    return pngUrl;
  }
  

  return (
    <div id="page-create-qr">
      <header >
        <h1>Gerador de Qr Codes</h1>
        <img src={Qr} alt="Ecoleta"/>
      </header>

      <main>
      <Link to="./PDF">
          <span>
              PDF
          </span>
          <strong>Olha o pdf</strong>
      </Link>

        <form onSubmit={handleSubmit}>
          <fieldset>
            <legend>
                <h2>Insira os dados</h2>
            </legend>
            <div className="field">
                <textarea
                  className="form-control"
                  id="exampleFormControlTextarea1"
                  rows={20} 
                  value={text}
                  onChange={handleInputChange}
                  />
            </div>
          </fieldset>
          <button type="submit"> Criar Qr Codes</button>
        </form>

        {items.length > 0 && 
          <fieldset>
            <legend>
                <h2>QrCodes prontos</h2>
                <span>Clique na imagem e faça download</span>
            </legend>
            <ul className="items-grid">
              {items.map(item => (
                <li key={item.id}>
                  <a onClick={()=> downloadQR(item.id)}>
                    <span>{item.id}. {item.lugar}</span>
                    <QRCode 
                      id={item.id}
                      value={JSON.stringify(item)}
                      includeMargin={true}
                      size={300}
                    />
                  </a>
                </li>
              ))}
            </ul>
          </fieldset>
        }  

      {items.length > 0 &&
      <PDFViewer height={900} width={800} >
            <Document>
            {items.map(item => (
              <Page key={item.id} style={styles.page} size="A4" page>
                <View style={styles.textWrapper}>
                  <Text style={styles.text}>
                  {item.id}. {item.lugar}
                  </Text>
                </View>
                <View style={styles.image}>
                  <Image src={()=>qrImg(item.id)}/>
                </View>
                <View style={styles.logo}>
                  <Image src={require('../../assets/logopet.png')}/>
                </View>
              </Page>
              ))}
            </Document>
            </PDFViewer>
      }
        </main>
    </div>
  );
}


export default App;

